package pendulum;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {

        HBox mainLayout = FXMLLoader.load(getClass().getResource("ui.fxml"));
        Scene scene = new Scene(mainLayout);

        primaryStage.setTitle("Маятник з рухомою точкою підвісу");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
