package pendulum;

public class Simulator {

    private double m1;
    private double m2;

    private double l;
    private double craneLen;

    private double initialHeadOffset; //x0
    private double initialRopeAngle; //a0
    private double initialHeadSpeed; //xx0
    private double initialRopeSpeed; //aa0

    private double timePeriod;
    private double h;
    private int n;

    private Coordinate[] headCoords;
    private Coordinate[] cargoCoords;

    private final double G = 9.80665;

    private boolean isCalculated = false;

    public void initCrane(double m1, double m2, double l, double craneLen) throws Exception {

        if (m1 <= 0) {
            throw new Exception("Wrong weight of crane's head");
        }
        this.m1 = m1;

        if (m2 <= 0) {
            throw new Exception("Wrong weight of cargo");
        }
        this.m2 = m2;

        if (l <= 0) {
            throw new Exception("Wrong length of rope");
        }
        this.l = l;

        if (craneLen <= 0) {
            throw new Exception("Wrong length of crane");
        }
        this.craneLen = craneLen;
    }

    public void initSimulation(double initialHeadOffset, double initialRopeAngle) throws Exception {

        initSimulation(initialHeadOffset, initialRopeAngle, 0, 0);
    }

    public void initSimulation(double initialHeadOffset, double initialRopeAngle,
                               double initialHeadSpeed, double initialRopeSpeed) throws Exception {

        if (initialHeadOffset <= 0 || initialHeadOffset > craneLen) {
            throw new Exception("Wrong offset of crane's head");
        }
        this.initialHeadOffset = initialHeadOffset;

        if (initialRopeAngle > Math.PI / 2 || initialRopeAngle < -Math.PI / 2) {
            throw new Exception("Wrong angle of rope");
        }
        this.initialRopeAngle = initialRopeAngle;

        if (initialHeadSpeed < 0) {
            throw new Exception("Wrong initial speed of crane's head");
        }
        this.initialHeadSpeed = initialHeadSpeed;

        if (initialRopeSpeed < 0) {
            throw new Exception("Wrong initial speed of rope");
        }
        this.initialRopeSpeed = initialRopeSpeed;
    }

    private void initTime(double timePeriod) {
        this.timePeriod = timePeriod;
        this.n = (int)timePeriod * 10;
        this.h = timePeriod / n;
    }

    private double f1(double x, double a, double p1, double p2) {
        return p2 / (m2 * l * Math.cos(a));
    }

    private double f2(double x, double a, double p1, double p2) {
        return (p1 * Math.cos(a) * l * m2 - p2 * m1 - p2 * m2) / (m2 * m2 * l * l * Math.cos(a) * Math.cos(a));
    }

    private double f3(double x, double a, double p1, double p2) {
        return 0;
    }

    private double f4(double x, double a, double p1, double p2) {
        return -p2 * p1 * Math.sin(a) / (m2 * l * Math.cos(a) * Math.cos(a))
                + m2 * l * l * a
                + p2 * p2 * (m1 + m2) * Math.sin(a) / (m2 * m2 * l * l * Math.pow(Math.cos(a), 3))
                - G * m2 * l * Math.sin(a);
    }

    private void calculateFunctions() {

        double P1[] = new double[n + 1];
        double P2[] = new double[n + 1];
        double X[] = new double[n + 1];
        double A[] = new double[n + 1];

        double K[][] = new double[4][4];

        X[0] = initialHeadOffset;
        A[0] = initialRopeAngle;
        P1[0] = initialHeadSpeed * m1;
        P2[0] = initialRopeSpeed * m2;

        for (int i = 0; i < n; i++) {

            K[0][0] = f1(X[i], A[i], P1[i], P2[i]) * h;
            K[1][0] = f2(X[i], A[i], P1[i], P2[i]) * h;
            K[2][0] = f3(X[i], A[i], P1[i], P2[i]) * h;
            K[3][0] = f4(X[i], A[i], P1[i], P2[i]) * h;

            K[0][1] = f1(X[i] + 0.5 * K[0][0], A[i] + 0.5 * K[1][0], P1[i] + 0.5 * K[2][0], P2[i] + 0.5 * K[3][0]) * h;
            K[1][1] = f2(X[i] + 0.5 * K[0][0], A[i] + 0.5 * K[1][0], P1[i] + 0.5 * K[2][0], P2[i] + 0.5 * K[3][0]) * h;
            K[2][1] = f3(X[i] + 0.5 * K[0][0], A[i] + 0.5 * K[1][0], P1[i] + 0.5 * K[2][0], P2[i] + 0.5 * K[3][0]) * h;
            K[3][1] = f4(X[i] + 0.5 * K[0][0], A[i] + 0.5 * K[1][0], P1[i] + 0.5 * K[2][0], P2[i] + 0.5 * K[3][0]) * h;

            K[0][2] = f1(X[i] + 0.5 * K[0][1], A[i] + 0.5 * K[1][1], P1[i] + 0.5 * K[2][1], P2[i] + 0.5 * K[3][1]) * h;
            K[1][2] = f2(X[i] + 0.5 * K[0][1], A[i] + 0.5 * K[1][1], P1[i] + 0.5 * K[2][1], P2[i] + 0.5 * K[3][1]) * h;
            K[2][2] = f3(X[i] + 0.5 * K[0][1], A[i] + 0.5 * K[1][1], P1[i] + 0.5 * K[2][1], P2[i] + 0.5 * K[3][1]) * h;
            K[3][2] = f4(X[i] + 0.5 * K[0][1], A[i] + 0.5 * K[1][1], P1[i] + 0.5 * K[2][1], P2[i] + 0.5 * K[3][1]) * h;

            K[0][3] = f1(X[i] + K[0][2], A[i] + K[1][2], P1[i] + K[2][2], P2[i] + K[3][2]) * h;
            K[1][3] = f2(X[i] + K[0][2], A[i] + K[1][2], P1[i] + K[2][2], P2[i] + K[3][2]) * h;
            K[2][3] = f3(X[i] + K[0][2], A[i] + K[1][2], P1[i] + K[2][2], P2[i] + K[3][2]) * h;
            K[3][3] = f4(X[i] + K[0][2], A[i] + K[1][2], P1[i] + K[2][2], P2[i] + K[3][2]) * h;

            X[i + 1] = X[i] + (1.0 / 6.0) * (K[0][0] + 2 * K[0][1] + 2 * K[0][2] + K[0][3]);
            A[i + 1] = A[i] + (1.0 / 6.0) * (K[1][0] + 2 * K[1][1] + 2 * K[1][2] + K[1][3]);
            P1[i + 1] = P1[i] + (1.0 / 6.0) * (K[2][0] + 2 * K[2][1] + 2 * K[2][2] + K[2][3]);
            P2[i + 1] = P2[i] + (1.0 / 6.0) * (K[3][0] + 2 * K[3][1] + 2 * K[3][2] + K[3][3]);
        }

        headCoords = new Coordinate[n + 1];
        for (int i = 0; i < n + 1; i++) {
            headCoords[i] = new Coordinate(X[i]);
        }

        cargoCoords = new Coordinate[n + 1];

        for (int i = 0; i < n + 1; i++) {
            cargoCoords[i] = new Coordinate(X[i] + l * Math.sin(A[i]), -l * Math.cos(A[i]));
        }

    }

    public void calculate(double timePeriod) throws Exception {

        if (timePeriod <= 0) {
            throw new Exception("Wrong time interval");
        }

        if (!isCalculated) {
            initTime(timePeriod);
            calculateFunctions();
            isCalculated = true;
        }
    }

    public void reset() {
        isCalculated = false;
    }

    public boolean isCalculated() {
        return isCalculated;
    }

    public Coordinate[] getHeadCoords() throws Exception {

        if (isCalculated) {

            return headCoords;
        } else {
            throw new Exception("Not calculated yet");
        }

    }

    public Coordinate[] getCargoCoords() throws Exception {

        if (isCalculated) {

            return cargoCoords;
        } else {
            throw new Exception("Not calculated yet");
        }
    }

    public int getN() {
        return n;
    }

    public int getNumberOfPoint() {
        return n + 1;
    }

    public double getH() {
        return h;
    }

    public double getL() {
        return l;
    }

    public double getCraneLen() {
        return craneLen;
    }

    public double getTime() {
        return timePeriod;
    }
}
