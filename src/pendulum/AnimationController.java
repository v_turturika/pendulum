package pendulum;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;


public class AnimationController {

    private Timeline animation;
    private int i=0;
    private Coordinate[] head;
    private Coordinate[] cargo;
    private LineChart<Number,Number> chart;
    private XYChart.Series<Number,Number> dataSeries;
    private int timePeriod;

    private Controller mainController;

    public AnimationController(Controller mainController) {
        this.mainController = mainController;
    }

    public void initAnimation(Coordinate[] head, Coordinate[] cargo, LineChart<Number,Number> chart, int time) {
        this.head = head;
        this.cargo = cargo;
        this.chart = chart;
        this.timePeriod = time;

        dataSeries = new XYChart.Series<>();

        XYChart.Data<Number,Number> headPoint = new XYChart.Data<>(head[0].getX(),head[0].getY());
        headPoint.setNode(new Rectangle(20,20));
        dataSeries.getData().add(headPoint);

        XYChart.Data<Number,Number> cargoPoint = new XYChart.Data<>(cargo[0].getX(),cargo[0].getY());

        Circle c = new Circle(20);

        Tooltip t = new Tooltip();
        c.setOnMousePressed(event -> {
            String str = String.format("(%1$f5; %2$f5)", cargo[i].getX(),cargo[i].getY());
            t.setText(str);
            t.show(c, event.getScreenX()+10,event.getScreenY()-10);
        });
        c.setOnMouseExited(event -> {
            t.hide();
        });


        cargoPoint.setNode(c);
        dataSeries.getData().add(cargoPoint);

        chart.getData().add(dataSeries);

        i=1;
        animation = new Timeline();
        animation.getKeyFrames().add(new KeyFrame(Duration.millis(100), (ActionEvent actionEvent) -> updateData()));
        animation.setCycleCount(timePeriod);
    }

    public void startAnimation() {
        animation.play();
    }

    public void pauseAnimation() {
        animation.pause();
    }

    public void restartAnimation() {

        chart.getData().clear();
        initAnimation(head,cargo,chart,timePeriod);
    }

    private void updateData() {

        if(i < timePeriod) {

            if(i % 10 == 0) {
                String currentTime = mainController.currentTime.getText();
                currentTime = currentTime.replaceAll("\\d+", String.valueOf(i/10));
                mainController.currentTime.setText(currentTime);
            }

            dataSeries.getData().get(0).setXValue(head[i].getX());
            dataSeries.getData().get(0).setYValue(head[i].getY());

            dataSeries.getData().get(1).setXValue(cargo[i].getX());
            dataSeries.getData().get(1).setYValue(cargo[i].getY());
            i++;
        }

    }

}
