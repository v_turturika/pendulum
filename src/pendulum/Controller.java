package pendulum;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML TextField headWeight;
    @FXML TextField cargoWeight;
    @FXML TextField ropeLen;
    @FXML TextField craneLen;
    @FXML TextField headOffset;
    @FXML TextField ropeAngle;
    @FXML TextField headSpeed;
    @FXML TextField ropeSpeed;
    @FXML TextField timePeriod;
    @FXML VBox chartContainer;
    @FXML Button startButton;
    @FXML Button pauseButton;
    @FXML Button restartButton;
    @FXML Label currentTime;

    LineChart<Number, Number> chart;
    NumberAxis xAxis;
    NumberAxis yAxis;

    AnimationController animController;
    Simulator craneSimulator;

    public Controller() {
        craneSimulator = new Simulator();
        animController = new AnimationController(this);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    private void clear(ActionEvent e) {
        headWeight.clear();
        cargoWeight.clear();
        ropeLen.clear();
        craneLen.clear();
        headOffset.clear();
        headSpeed.clear();
        ropeSpeed.clear();
        ropeAngle.clear();
        timePeriod.clear();
        chartContainer.getChildren().remove(chart);
        startButton.setDisable(true);
        pauseButton.setDisable(true);
        restartButton.setDisable(true);
        currentTime.setText("Пройшло часу : 00 c.");
    }

    @FXML
    private void simulate(ActionEvent event) {

        craneSimulator.reset();
        chartContainer.getChildren().remove(chart);
        calculate();
        try {
               if(craneSimulator.isCalculated()) {
                initChart();
                animController.initAnimation(craneSimulator.getHeadCoords(), craneSimulator.getCargoCoords(),
                                             chart, craneSimulator.getNumberOfPoint());
                startButton.setDisable(false);
            }
        }
        catch (Exception e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.CLOSE).showAndWait();
        }

    }
    @FXML
    private void startAnimation(ActionEvent event) {
        animController.startAnimation();
        startButton.setDisable(true);
        pauseButton.setDisable(false);
        restartButton.setDisable(true);

    }

    @FXML
    private void pauseAnimation(ActionEvent event) {
        animController.pauseAnimation();
        pauseButton.setDisable(true);
        startButton.setDisable(false);
        restartButton.setDisable(false);
    }

    @FXML
    private void restartAnimation(ActionEvent event) {
        animController.restartAnimation();
        restartButton.setDisable(true);
        currentTime.setText("Пройшло часу : 00 c.");
    }

    private void initChart() {

        xAxis = new NumberAxis(0,craneSimulator.getCraneLen(),1);
        yAxis = new NumberAxis(-craneSimulator.getL()-1,0,1);

        chart = new LineChart<>(xAxis,yAxis);

        chart.setLegendVisible(false);
        chart.setAnimated(false);
        chartContainer.getChildren().add(chart);
    }

    private void calculate() {

        try{
            double headWeight = Double.valueOf(this.headWeight.getText()),
                   cargoWeight = Double.valueOf(this.cargoWeight.getText()),
                   ropeLen = Double.valueOf(this.ropeLen.getText()),
                   craneLen = Double.valueOf(this.craneLen.getText()),
                   headOffset = Double.valueOf(this.headOffset.getText()),
                   ropeAngle = Double.valueOf(this.ropeAngle.getText())*Math.PI/180,
                   headSpeed = Double.valueOf(this.headSpeed.getText()),
                   ropeSpeed = Double.valueOf(this.ropeSpeed.getText()),
                   time = Double.valueOf(this.timePeriod.getText());

            craneSimulator.reset();
            craneSimulator.initCrane(headWeight,cargoWeight,ropeLen,craneLen);
            craneSimulator.initSimulation(headOffset,ropeAngle,headSpeed,ropeSpeed);
            craneSimulator.calculate(time);
        }
        catch (Exception e) {
            new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.CLOSE).showAndWait();
            craneSimulator.reset();
        }
    }
}
